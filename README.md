# Hothouse Docker WordPress

This is a simple WordPress Docker boilerplate that you can use for your projects. If you like it, maybe FORK it and use it to jump start your project.

### Returning Dev Steps
1. Navigate to the "www" folder in terminal
2. Run "docker-compose up"
3. Open "http://localhost:8000" to see the site
4. You can make changes to stuff in the "wp-content" folder and you will see it updated in the browser (after you refresh).

### Initial Setup
0. Make sure you have Docker installed
1. Open the "docker-compose.yml" file in your editor
2. In the "wordpress" service, comment out the "volumes" line and the line below that
3. Run "docker-compose up -d"
4. Check to make sure WP is running (probably http://localhost:8000). If it is running and can connect to the DB container you should see the WP setup screen (do not begin that process yet).
5. Run "docker container ls" and find the container ID for the WP instance
6. Run "docker cp CONTAINER_ID:/var/www/html/wp-content ./wp-content" (but substitute the container ID you found)
7. You should see a new "wp-content" folder appear
8. Stop the instance by running "docker stop CONTAINER_ID" (but substitute the container ID you found)
9. In the "docker-compose.yml", un-comment out the "wp-content" volume mount in the "db" service
10. Run "docker-compose up" again
11. The container will now be use the "wp-content" folder that was created
12. Browse to the instance (probably http://localhost:8000) and do the normal WP install (or use your MySQL client to import an existing DB instead)

### Tips
1. You can change the MySQL port to something other than 3306 if you already have MySQL running on localhost (maybe because you have MAMP/XAMP/WAMP running). To use a different port, change the first value in the "db" service's port mapping and also update the value in the "wordpress" service's environment variable.
2. Since the database data is stored on a "Docker Volume", it will persist locally. However, if you want to share that data with others you should connect to the database with your MySQL client and make a DB dump file (and maybe commit to the repo if appropriate).
3. If you are on Windows and can't access localhost, it's probably because you installed "Docker Toolbox" instead of "Docker Desktop for Windows" (https://docs.docker.com/toolbox/toolbox_install_windows/). If your system supports "Docker Desktop for Windows", you should use that instead. If not, you should go get a new computer.
